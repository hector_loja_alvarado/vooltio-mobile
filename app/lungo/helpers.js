var Helpers = (function(lng, undefined){
    // Changes XML to JSON
    function xmlToJson(xml) {
        // Create the return object
        var obj = {};

        if (xml.nodeType == 1) { // element
            // do attributes
            if (xml.attributes.length > 0) {
            obj["_attributes"] = {};
                for (var j = 0; j < xml.attributes.length; j++) {
                    var attribute = xml.attributes.item(j);
                    obj["_attributes"][attribute.nodeName] = attribute.nodeValue;
                }
            }
        } else if (xml.nodeType == 3) { // text
            obj = xml.nodeValue;
        }

        // do children
        if (xml.hasChildNodes()) {
            for(var i = 0; i < xml.childNodes.length; i++) {
                var item = xml.childNodes.item(i);
                var nodeName = item.nodeName;
                if (typeof(obj[nodeName]) == "undefined") {
                    obj[nodeName] = xmlToJson(item);
                } else {
                    if (typeof(obj[nodeName].push) == "undefined") {
                        var old = obj[nodeName];
                        obj[nodeName] = [];
                        obj[nodeName].push(old);
                    }
                    obj[nodeName].push(xmlToJson(item));
                }
            }
        }
        return obj;
    };

    function getLotFromTime(date){
        console.log("4 "+ date.getHours());
        return ""
    }

    function getTramoString(tramo){
        switch(tramo)
        {
            case 0: return "00-01"; break;
            case 1: return "01-02"; break;
            case 2: return "02-03"; break;
            case 3: return "03-04"; break;
            case 4: return "04-05"; break;
            case 5: return "05-06"; break;
            case 6: return "06-07"; break;
            case 7: return "07-08"; break;
            case 8: return "08-09"; break;
            case 9: return "09-10"; break;
            case 10: return "10-11"; break;
            case 11: return "11-12"; break;
            case 12: return "12-13"; break;
            case 13: return "13-14"; break;
            case 14: return "14-15"; break;
            case 15: return "15-16"; break;
            case 16: return "16-17"; break;
            case 17: return "17-18"; break;
            case 18: return "18-19"; break;
            case 19: return "19-20"; break;
            case 20: return "20-21"; break;
            case 21: return "21-22"; break;
            case 22: return "22-23"; break;
            case 23: return "23-24"; break;
        }
    }


    function actualPrice(date){    
        valores = __Model.Precio.all();                
        result = null;
        now = new Date();
        tramo = getTramoString(now.getHours());
        for (_k = 0, _len2 = valores.length; _k < _len2; _k++) {
            precio = valores[_k];
            if (precio.fecha === date && precio.tramo === tramo) {  
                result = precio;
            } 
        }   
        return result;   
    }    

    function minPrice(date){   
        valores = __Model.Precio.all();        
        maxValue = 9999;
        result = null;

        for (_k = 0, _len2 = valores.length; _k < _len2; _k++) {
            precio = valores[_k];
            if (precio.fecha === date && parseFloat(precio.valor) < maxValue) {
                maxValue = parseFloat(precio.valor);
                result = precio;
            } 
        }   
        return result;          
    }

    function maxPrice(date){
        valores = __Model.Precio.all();        
        maxValue = 0;
        result = null;

        for (_k = 0, _len2 = valores.length; _k < _len2; _k++) {
            precio = valores[_k];
            if (precio.fecha === date && parseFloat(precio.valor) > maxValue) {
                maxValue = parseFloat(precio.valor);
                result = precio;
            } 
        }   
        return result;     
    }

    function getSeriePrice(date){
        valores = __Model.Precio.all();        
        result = [];

        for (_k = 0, _len2 = valores.length; _k < _len2; _k++) {
            precio = valores[_k];
            if (precio.fecha === date) {
                //result.push(parseFloat(precio.valor.replace(',', '.')));
                result.push(precio.valor);
            } 
        }   
        return result;     
    }

    function findTramoById(id){
        tramos = __Model.Tramo.all();
        result = null
        for (_k = 0, _len2 = tramos.length; _k < _len2; _k++) {
            tramo = tramos[_k];            
            if(tramo.id === id)
                result = tramo
        }   
        return result;   

    }

    function getSerieTramos(){
        tramos = __Model.Tramo.all();
        result = [];
        for (_k = 0, _len2 = tramos.length; _k < _len2; _k++) {
            tramo = tramos[_k];            
            result.push(tramo.valor);
        }   
        return result;   
    }

    function toKilowatio(value, decimals){
        return (parseFloat(value)/1000).toFixed(decimals).toString()/*.replace(".",",")*/
    }

    function checkDate(date){
        valores = __Model.Precio.all();        
        result = false;

        for (_k = 0, _len2 = valores.length; _k < _len2; _k++) {
            precio = valores[_k];
            if (precio.fecha === date) {
                result = true;
                break;
            } 
        }   
        return result;
    }




    return {
        checkDate: checkDate,
        toKilowatio: toKilowatio,
        xmlToJson: xmlToJson,
        getLotFromTime: getLotFromTime,
        maxPrice: maxPrice,
        minPrice: minPrice,
        actualPrice: actualPrice,
        findTramoById: findTramoById,
        getSerieTramos: getSerieTramos,
        getSeriePrice: getSeriePrice,
        getTramoString: getTramoString,        
    }
})(Lungo);