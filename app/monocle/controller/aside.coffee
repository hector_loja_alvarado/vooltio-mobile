class AsideCtrl extends Monocle.Controller

    events:
        "load aside#options"  : "onLoadAside"
        "tap li"    :   "onChangeSection"

    onLoadAside: (event) ->
        console.log "onLoadAside"        

    onChangeSection: (event) ->
        console.log "onChangeSection"     
        
        Lungo.dom("li.active").removeClass()
        Lungo.dom("li#"+event.currentTarget.id).addClass('active')
        
        Lungo.Router.aside(event.currentTarget.id, "options");
        Lungo.Router.section(event.currentTarget.id)   

controller_aside = new AsideCtrl "aside#options"