class CalculatorCtrl extends Monocle.Controller

    events:
        "load article#calculator-energy-2"  : "onLoad"

    onLoad: (event) ->
        console.log "onLoad article#calculator-energy-2"

        __Model.Electrodomestico.create id:1, cuenta:0, descripcion:"Aire Acondicionado", potencia: 2000
        __Model.Electrodomestico.create id:2, cuenta:0, descripcion:"Bombillas", potencia: 60
        __Model.Electrodomestico.create id:3, cuenta:0, descripcion:"Bombillas Bajo Consumo", potencia: 9
        __Model.Electrodomestico.create id:4, cuenta:0, descripcion:"Calefacción eléctrica", potencia: 2500
        #__Model.Electrodomestico.create descripcion:"Calefacción eléctrica de bajo consumo", potencia: 800
        __Model.Electrodomestico.create id:5, cuenta:0, descripcion:"Equipos ofímaticos", potencia: 250
        __Model.Electrodomestico.create id:6, cuenta:0, descripcion:"Frigorífico", potencia: 350
        __Model.Electrodomestico.create id:7, cuenta:0, descripcion:"Horno", potencia: 2200            
        __Model.Electrodomestico.create id:8, cuenta:0, descripcion:"Lavadora", potencia: 2200
        __Model.Electrodomestico.create id:9, cuenta:0, descripcion:"Lavavajillas", potencia: 2200
        __Model.Electrodomestico.create id:10, cuenta:0, descripcion:"Microondas", potencia: 1500
        __Model.Electrodomestico.create id:11, cuenta:0, descripcion:"Pequeños electrodomésticos", potencia: 1500
        __Model.Electrodomestico.create id:12, cuenta:0, descripcion:"Secadora", potencia: 2500
        __Model.Electrodomestico.create id:13, cuenta:0, descripcion:"Televisor", potencia: 400
        __Model.Electrodomestico.create id:14, cuenta:0, descripcion:"Vitrocerámica", potencia: 2000     

        $$("ul#appliances").html('')

        for electrodomestico in __Model.Electrodomestico.all()
            view = new __View.ElectrodomesticoItemList model: electrodomestico
            view.append electrodomestico

controller_calculator = new CalculatorCtrl "article#calculator-energy-2"