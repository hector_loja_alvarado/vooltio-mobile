class MainCtrl extends Monocle.Controller

    events:
        "load section#main" : "onLoadMain"
        "tap div#grafico"   : "onShowGrafico"
        "tap div#tabla"     : "onShowTabla"
        "tap a#today"       : "onChangeDay"
        "tap a#tomorrow"    : "onChangeDay"        

    elements:
        "h1#price"          : "price"
        "h4#mensaje"        :   "time"
        "span#maxPrice"     : "maxPrice"
        "span#minPrice"     : "minPrice"   
        "span#tramoMax"     : "tramoMax"
        "span#tramoMin"     : "tramoMin"   


    onChangeDay: (event) ->
        console.log "onChangeDay"

        if Helpers.checkDate Date.parse(event.currentTarget.id).compressedDate()        
            Lungo.dom("a.active").removeClass('active')
            Lungo.dom("a#"+event.currentTarget.id).addClass('active')

        
            if event.currentTarget.id is "tomorrow"
                @time[0].innerHTML  = "Mañana el kilowatio por hora valdrá"
            else
                @time[0].innerHTML  = "Ahora mismo el kilowatio por hora vale"                                       


            Lungo.Data.Storage.persistent("dateSelected",event.currentTarget.id)
            
            loadData(Date.parse(event.currentTarget.id).compressedDate())        

        else
            Lungo.Notification.html('<div id="modal"><span class="margin4x"><h2>Todavía no están los datos disponibles.</h2></span></div>', "Cerrar");

    onShowGrafico: (event) ->
        console.log "onShowGrafico"

        Lungo.dom('h4#linkGrafico').addClass('bold')
        Lungo.dom('h4#linkTabla').removeClass('bold')

        Lungo.dom('div#containerGrafico').show()
        Lungo.dom('div#containerTabla').hide()        

    onShowTabla: (event) ->
        console.log "onShowTabla"        

        Lungo.dom('h4#linkTabla').addClass('bold')
        Lungo.dom('h4#linkGrafico').removeClass('bold') 

        Lungo.dom('div#containerTabla').show()
        Lungo.dom('div#containerGrafico').hide()

    onLoadMain: (event) ->
        console.log "onLoadMain"

        
        Lungo.dom("a.active").removeClass('active')
        Lungo.dom("a#"+Lungo.Data.Storage.persistent("dateSelected")).addClass('active')


        loadData(Date.parse(Lungo.Data.Storage.persistent("dateSelected")).compressedDate())
        ###
        today = Date.parse('today')
        todayCompressed = today.compressedDate()
        tomorrow = Date.parse('tomorrow')
        tomorrowCompressed = tomorrow.compressedDate()
        now = new Date()

        Lungo.Service.Settings.error = errorResponse

        url = "http://www.vooltio.es/api/v1/serie/precios.json"
        post_data =
            fecha: todayCompressed,
            tarifa: 0        

        result = Lungo.Service.get(url, post_data, preciosResponse)            

        post_data =
            fecha: tomorrowCompressed,            
            tarifa: 0

        result = Lungo.Service.get(url, post_data, preciosManyanaResponse)        
        ###
        
    preciosResponse= (response) ->
        console.log "preciosResponse"

        for precio in response        
            precio = new __Model.Precio tramo: precio.tramo, valor:precio.valor, fecha: precio.fecha
            precio.save()

        if Lungo.Data.Storage.persistent("dateSelected") == "today"
            loadData(Date.parse('today').compressedDate())

    preciosManyanaResponse= (response) ->
        console.log "preciosManyanaResponse"

        for precio in response        
            console.log precio
            precio = new __Model.Precio tramo: precio.tramo, valor:precio.valor, fecha: precio.fecha
            precio.save()    

        if Lungo.Data.Storage.persistent("dateSelected") == "tomorrow"
            loadData(Date.parse('today').compressedDate())                        

    loadData= (date) ->
        console.log "loadData"

        console.log date
        actualPrice = Helpers.actualPrice(date)        
        @price.innerHTML = actualPrice.valor + "€"

        
        tramoMaximo = Helpers.maxPrice(date)        
        @maxPrice.innerHTML = tramoMaximo.valor + " €"
        splitMax = tramoMaximo.tramo.split "-"
        @tramoMax.innerHTML = "Desde las <span class='text bold'>" + splitMax[0] + "</span> a las <span class='text bold'>"+ splitMax[1] + "</span> horas."


        tramoMinimo = Helpers.minPrice(date)        
        @minPrice.innerHTML = tramoMinimo.valor + " €"
        splitMin = tramoMinimo.tramo.split "-"
        @tramoMin.innerHTML = "Desde las <span class='text bold'>" + splitMin[0] + "</span> a las <span class='text bold'>"+ splitMin[1] + "</span> horas."
    
        refreshChart(date)
        #refreshTable(date)

    refreshTable= (date) ->
        console.log "onrefreshTable"

        $$("ul#precios").html('')

        tramoMinimo = Helpers.minPrice(date) 
        flagGreen = (tramoMinimo.valor*5/100) + tramoMinimo.valor

        tramoMaximo = Helpers.maxPrice(date)         
        flagRed = tramoMaximo.valor - (tramoMaximo.valor*5/100)


        for precio in __Model.Precio.all()            
            if precio.fecha == date
                precio.flag = "warning"
                if precio.valor < flagGreen
                    precio.flag = "accept"
                else if precio.valor > flagRed
                    precio.flag = "cancel"

                view = new __View.PriceItemList model: precio
                view.append precio


    refreshChart= (date) ->    
        lineChartData = {
            labels : ["0h","1h","2h","3h","4h","5h","6h","7h","8h","9h","10h","11h","12h","13h","14h","15h","16h","17h","18h","19h","20h","21h","22h","23h"],
            datasets : [
                {                    
                    strokeColor : "#B1C635",
                    pointColor : "#FFFFFF",
                    pointStrokeColor : "#B1C635",
                    data : Helpers.getSeriePrice(date)
                }
                
            ]

        }

        options = {   

            #String - Colour of the scale line 
            scaleLineColor : "rgba(0,0,0,.4)",
            
            #Number - Pixel width of the scale line    
            scaleLineWidth : 1,

            #Boolean - Whether to show labels on the scale 
            scaleShowLabels : true,
            
            #Interpolated JS string - can access value
            scaleLabel : "<%=value%>",
            
            #String - Scale label font declaration for the scale label
            scaleFontFamily : "'Arial'",
            
            #Number - Scale label font size in pixels  
            scaleFontSize : 10,
            
            #String - Scale label font weight style    
            scaleFontStyle : "normal",
            
            #String - Scale label font colour  
            scaleFontColor : "rgba(0,0,0,.4)",    
            
            #Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines : true,
            
            #String - Colour of the grid lines
            scaleGridLineColor : "rgba(0,0,0,.05)",
            
            #Number - Width of the grid lines
            scaleGridLineWidth : 1, 
            
            #Boolean - Whether the line is curved between points
            bezierCurve : false,
            
            #Boolean - Whether to show a dot for each point
            pointDot : true,
            
            #Number - Radius of each point dot in pixels
            pointDotRadius : 4,
            
            #Number - Pixel width of point dot stroke
            pointDotStrokeWidth : 2,
            
            #Boolean - Whether to show a stroke for datasets
            datasetStroke : false,
            
            #Number - Pixel width of dataset stroke
            datasetStrokeWidth : 4,
            
            #Boolean - Whether to fill the dataset with a colour
            datasetFill : false,
            
            #Boolean - Whether to animate the chart
            animation : true,

            #Number - Number of animation steps
            animationSteps : 60,
            
            #String - Animation easing effect
            animationEasing : "easeOutQuart",

            #Function - Fires when the animation is complete
            onAnimationComplete : null
            
        }
        
        canvas = Lungo.dom("#canvas")[0]
        container_canvas = Lungo.dom("#chart")[0]
        canvas.width = container_canvas.clientWidth
        canvas.height = container_canvas.clientHeight
        chart = (canvas).getContext("2d")
        myLine = new Chart(chart).Line(lineChartData, options);                    
        

    errorResponse= (type, xhr) ->
        console.log "errorResponse"
        console.log("xhr: %o", xhr.response)        

controller_main = new MainCtrl "section#main"