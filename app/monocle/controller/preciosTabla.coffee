class PreciosTablaCtrl extends Monocle.Controller

    events:
        "load section#precios_tabla"    : "onLoadSection"
        "tap a#today"                   : "onChangeDay"
        "tap a#tomorrow"                : "onChangeDay" 
        "tap h4#tipo_orden"             : "onChangeSortBy"           
        "tap span#orderUp"              : "onChangeOrder"
        "tap span#orderBottom"          : "onChangeOrder"

    elements:
        "span#labelSortBy"     : "labelSortBy"
        "span#orderUp"         : "orderUp"
        "span#orderBottom"     : "orderBottom"


    onChangeOrder: (event) ->
        console.log "onChangeOrder"

        if Lungo.Data.Storage.persistent("sort") == "DESC"
            Lungo.Data.Storage.persistent("sort", "ASC")
            @orderBottom.removeClass('color-theme2')
            @orderUp.addClass('color-theme2')
        else
            Lungo.Data.Storage.persistent("sort", "DESC")
            @orderUp.removeClass('color-theme2')
            @orderBottom.addClass('color-theme2')        

        refreshData()

    onChangeSortBy: (event) ->
        console.log "onChangeSortBy"

        $$("ul#listado").html('')

        if Lungo.Data.Storage.persistent("order_by") == "hora"
            @labelSortBy[0].innerHTML  = "Precio"
            Lungo.Data.Storage.persistent("order_by", "valor")
        else
            Lungo.Data.Storage.persistent("order_by") == "valor"
            @labelSortBy[0].innerHTML  = "Hora"
            Lungo.Data.Storage.persistent("order_by", "hora")

        refreshData()

    onLoadSection: (event) ->
        console.log "onLoadSection"

        Lungo.dom("a.active").removeClass('active')
        Lungo.dom("a#"+Lungo.Data.Storage.persistent("dateSelected")).addClass('active')

        date = Date.parse(Lungo.Data.Storage.persistent("dateSelected")).compressedDate()
        loadData(date)
        

    onChangeDay: (event) ->
        console.log "onChangeDay"

        if Helpers.checkDate Date.parse(event.currentTarget.id).compressedDate()        
            Lungo.dom("a.active").removeClass('active')
            Lungo.dom("a#"+event.currentTarget.id).addClass('active')
                                             
            Lungo.Data.Storage.persistent("dateSelected",event.currentTarget.id)
            
            loadData(Date.parse(event.currentTarget.id).compressedDate())        

        else
            Lungo.Notification.html('<div id="modal"><span class="margin4x"><h2>Todavía no están los datos disponibles.</h2></span></div>', "Cerrar");                                               

    refreshData= () ->
        date = Date.parse(Lungo.Data.Storage.persistent("dateSelected")).compressedDate()
        loadData(date)

    loadData= (date) ->
        console.log "loadData"

        $$("ul#listado").html('')

        tramoMinimo = Helpers.minPrice(date) 
        flagGreen = (tramoMinimo.valor*5/100) + tramoMinimo.valor

        tramoMaximo = Helpers.maxPrice(date)         
        flagRed = tramoMaximo.valor - (tramoMaximo.valor*5/100)


        for precio in sortData(__Model.Precio.all())
            if precio.fecha == date
                precio.flag = "warning"
                if precio.valor < flagGreen
                    precio.flag = "accept"
                else if precio.valor > flagRed
                    precio.flag = "cancel"

                view = new __View.PriceTableItemList model: precio       
                view.append precio           

    sortBy= (key, a, b, r) ->
        r = if r then 1 else -1
        return -1*r if a[key] > b[key]
        return +1*r if a[key] < b[key]
        return 0

    sortData= (data) ->
        console.log "SORT"
        console.log Lungo.Data.Storage.persistent("order_by")
        console.log Lungo.Data.Storage.persistent("sort")

        data.sort (a, b) ->
            sort = Lungo.Data.Storage.persistent("sort") 

            if sort == "ASC"
                sortBy(Lungo.Data.Storage.persistent("order_by"), a, b)
            else
                sortBy(Lungo.Data.Storage.persistent("order_by"), b, a)

        console.log data
        data


controller_main = new PreciosTablaCtrl "section#precios_tabla"        