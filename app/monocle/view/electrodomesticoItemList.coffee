class __View.ElectrodomesticoItemList extends Monocle.View

    container: "ul#appliances"

    template:
        """
            <li id="element_{{id}}" class="thumb">
                <img src="assets/images/icons/{{id}}.png">
                <div>
                    <div id="cuenta" class="on-right text bold" style="font-size: 2.6em;"></div>
                    <strong>{{descripcion}}</strong>
                    <small>{{potencia}} vatios</small>
                </div>
            </li>
        """

    elements:
        "div#cuenta"    : "cuenta"

    events:
        "tap li"    : "onTap"
        "swipeLeft li"   : "onRemove"

    onRemove: (event) ->
        console.log "onRemove"

        if @model.cuenta > 0
            @model.cuenta = @model.cuenta - 1
            @model.save()
            e = __Model.Electrodomestico.find(@model.uid)
            e.cuenta = @model.cuenta
            e.save()        

            if @model.cuenta == 0
                @cuenta.html('')
                Lungo.dom("li#element_"+@model.id).removeClass('theme')
            else    
                @cuenta.html(@model.cuenta)



    onTap: (event) ->
        console.log "onTap"

        @model.cuenta = @model.cuenta + 1
        @model.save()
        @cuenta.html(@model.cuenta)
        
        e = __Model.Electrodomestico.find(@model.uid)
        e.cuenta = @model.cuenta
        e.save()        

        Lungo.dom("li#element_"+@model.id).addClass("theme")