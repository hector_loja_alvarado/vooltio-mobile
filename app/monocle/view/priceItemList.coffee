class __View.PriceItemList extends Monocle.View

	container: "ul#precios"

	template:
		"""
	        <li class="{{flag}}">
	            <div class="right">{{valor}} €</div>
	            <strong>{{tramo}}</strong>
	        </li>		
		"""