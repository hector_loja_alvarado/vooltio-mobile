class __View.PriceTableItemList extends Monocle.View

	container: "ul#listado"

	template:
		"""
        <li style="width:45%; float:left; padding: 0px; margin-left: 5px; margin-right: 5px" class="text center">
            <div class="text bold color {{flag}}" style=" font-size: 1.5em; padding: 10px; color: #FFF">
                <strong>{{valor}} €</strong>
            </div>
            <div class="border {{flag}}" style="padding: 5px; font-size: 1.3em;"">
                <small>{{tramo}} horas</small>
            </div>
        </li>
		"""